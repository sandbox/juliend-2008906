<?php

$plugin = array(
  'title' => t('Priceminister'),
  'description' => t('Priceminister marketplace'),
  'class' => 'CommercePriceministerMarketplace',
  'configurable' => TRUE,
  'status' => FALSE,
);
