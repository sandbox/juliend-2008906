<?php

/**
 *
 */


/**
 *
 */
class CommercePriceministerMarketplace extends CommerceMarketplacesMarketplace {

  public function __construct(array $values) {
    parent::__construct($values, 'commerce_usage_cycle_type');
  }

  public function configDefaults() {
    return array(
      'login' => '',
      'pwd' => '',
      'version' => '',
      'mode' => 'test',
    );
  }

  public function configForm($form, &$form_state, $marketplace) {
    // Merge default settings with the stored settings array.
    $marketplace->config += $this->configDefaults();
    // Login.
    $form['login'] = array(
      '#type' => 'textfield',
      '#title' => t('Login'),
      '#default_value' => $marketplace->getConfig('login'),
      '#required' => TRUE,
    );
    // Password.
    $form['pwd'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#default_value' => $marketplace->getConfig('pwd'),
      '#required' => TRUE,
    );
    // Version.
    $form['version'] = array(
      '#type' => 'textfield',
      '#title' => t('Key version'),
      '#description' => t('the version of the server.'),
      '#default_value' => $marketplace->getConfig('version'),
      '#required' => TRUE,
    );
    $form['mode'] = array(
      '#type' => 'radios',
      '#title' => t('Mode'),
      '#options' => array(
        'test' => ('Test - allow you to test in real conditions on Priceminister sandboxes'),
        'live' => ('Live - process real export import to a live account'),
      ),
      '#default_value' => $marketplace->getConfig('mode'),
    );
    return $form;
  }

  public function configFormValidate($form, &$form_state) {
    parent::configFormValidate($form, $form_state);
  }

  public function configFormSubmit($form, &$form_state) {
    parent::configFormSubmit($form, $form_state);
  }

  public function getProductTypes() {

    $product_types = get_product_types($this->getConfig());
    return $product_types;
    dsm($product_types, '$product_types');

    $target_fields = array(
      'categ1' => t('categ 1'),
      'categ2' => t('categ 2'),
      'categ3' => t('categ 3'),
    );

    return $target_fields;
  }

  public function getProductTypeFields($product_type) {
    $product_types = get_product_types_fields($product_type, $this->getConfig());
    return $product_types;
  }
}

class commercePriceministerField {

  public function __construct() {

  }


  // Declare the mapping between Priceminister fields form and Commerce marketplace
  // Marketplace => Commerce Marketplace
  protected function fieldsMapping() {
    return array(
      'label' =>  'label',
      'key' => 'name',
      'mandatory' => 'required',
      'multivalued' => 'multiple',
    );
  }

  public function convertFieldToPriceministerFormat() {

  }

  public function convertFieldToDrupalCommerceFormat() {

  }
}